#pragma once

#ifdef G8_HOST
#define OPEN __declspec(dllimport)
#else
#define OPEN __declspec(dllexport)
#endif

namespace obj
{
    class OPEN code
    {
    public:
        void init()
        {
            
        }
    };

    code* OPEN load();
}
