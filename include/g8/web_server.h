#pragma once

#include <string>

#include <yhirose/httplib.h>

#include "../common/console.h"

#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

namespace webserver
{

void start(std::string p);
} // namespace webserver
