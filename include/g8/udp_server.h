#pragma once

// this definition is made here as redefinition is not an issue and it is
// needed in the declarations.
#define NETPACK_BUFFER_SIZE 128

#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>
#include <netpack/netpack.hpp>

#include "../common/console.h"
#include "../common/terminal.h"

namespace udpserver
{
struct user;
struct session;

void err_handle(int);

void on_authed(netpack::endpoint *);
void on_new_connection(netpack::endpoint *);
void on_recieve(const char *, netpack::endpoint *);
void on_response(const char *, netpack::endpoint *);

void session_command(std::string);

int start();
} // namespace udpserver
