#include <windows.h>
#include <stdio.h>

#include <string>
#include <iostream>

namespace plugin
{
    typedef void*(__cdecl* MYPROC)();

    void* load(std::string path)
    {
        HINSTANCE plug_handle;
        MYPROC func;

        // Get a handle to the DLL module.
        plug_handle = LoadLibraryA(path.c_str());

        // If the handle is valid, try to get the function address.
        if (plug_handle != NULL)
        {
            func = (MYPROC) GetProcAddress(plug_handle, "init");
            if (func == NULL)  std::cout << "Failed to get function address" << std::endl;
            else func();
        }
        else std::cout << "Failed to load DLL" << std::endl;

        FreeLibrary(plug_handle);
    }
}