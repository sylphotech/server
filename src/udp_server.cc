#define NETPACK_DEFINITION
#define TERMINAL_DEFINITION

#include "g8/udp_server.h"

namespace udpserver
{

struct session
{
    std::vector<user *> users;
    std::vector<std::string> files;
};

struct user
{
    std::string name;

    session *sess;

    netpack::endpoint *end;

    user(netpack::endpoint *e, session *s)
    {
        end = e;
        sess = s;
    }
};

bool running = true;

std::unordered_map<netpack::endpoint *, user *> user_lookup;
std::unordered_map<std::string, session *> sessions;

session *default_session = new session();

void err_handle(int err_code)
{
    exit(err_code);
}

void on_authed(netpack::endpoint *end)
{
    // wait what? :lmfao:
}

void on_new_connection(netpack::endpoint *end)
{
    std::cout << "Cli connected with auth " << end->auth_token << std::endl;
    user *usr = new user(end, default_session);
    user_lookup.insert(
        std::unordered_map<netpack::endpoint *, user *>::value_type(end, usr));
}

void on_recieve(const char *data, netpack::endpoint *end)
{
    std::cout << "Received inbound: " << data << std::endl;
}

void on_response(const char *data, netpack::endpoint *end)
{
    std::cout << data << std::endl;
}

void session_command(std::string msg)
{
    std::vector<std::string> args;
    std::size_t pos = 0;
    while ((pos = msg.find(' ')) != std::string::npos)
    {
        args.push_back(msg.substr(0, pos));
        msg = msg.substr(pos);
    }

    if (args.size() == 0)
    {
        std::cout << "[Session]" << std::endl;
        for (auto sess : sessions)
            std::cout << sess.first << std::endl;
    }
    else if (args.size() == 1)
    {
        if (args.at(0) == "help")
        {
            std::cout << "[Session Help]" << std::endl;
            std::cout << "session               - Lists all active sessions." << std::endl;
            std::cout << "session users <name>  - Lists a sessions users." << std::endl;
            std::cout << "session create <name> - Creates a new session." << std::endl;
            std::cout << "session close <name>  - Closes a session." << std::endl;
            std::cout << "session reset <name>  - Resets a session." << std::endl;
            std::cout << "session lock <name>   - Locks a session." << std::endl;
            std::cout << "session unlock <name> - Unlocks a session." << std::endl;
        }
    }
    else if (args.size() == 2)
    {
        if (args.at(0) == "users")
        {
            // TODO
        }
        else if (args.at(0) == "create")
        {
            // TODO
        }
        else if (args.at(0) == "close")
        {
            // TODO
        }
        else if (args.at(0) == "reset")
        {
            // TODO
        }
        else if (args.at(0) == "lock")
        {
            // TODO
        }
        else if (args.at(0) == "unlock")
        {
            // TODO
        }
    }
}

int start()
{
    netpack::auth = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx";

    netpack::on_new_connection = &on_new_connection;
    netpack::on_recieve = &on_recieve;
    netpack::on_response = &on_response;
    netpack::on_authed = &on_authed;
    netpack::port = 20454;

    // terminate if we fail to start
    if (!netpack::start())
    {
        std::cout << "failed to start!" << std::endl;
        return 0;
    }

    terminal::create_command(
        "session", session_command,
        "Show session info. Use `session help` for more."
    );

    sessions.insert(std::unordered_map<std::string, session *>::value_type(
        "default", default_session));

    // we combine manually as std::cout << can be broken up between threads
    std::string msg = "UDP Server on port [";
    msg += std::to_string(netpack::port) + "]\n";
    std::cout << msg;

    while (running) netpack::recieve();

    netpack::clean();

    return 0;
}
} // namespace udpserver
