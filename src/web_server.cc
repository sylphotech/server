#include "g8/web_server.h"

namespace webserver
{
using namespace httplib;

std::string path;

Server svr;

void start(std::string p)
{
    path = p;

    svr.Get("/ping", [](const Request &req, Response &res) {
        res.set_content("Pong!", "text/plain");
    });

    svr.set_base_dir(path.c_str());

    int port = svr.bind_to_any_port("0.0.0.0");

    // we combine manually as std::cout << can be broken up between threads
    std::string msg = "HTTP Server on port [";
    msg += std::to_string(port) + "]\n";
    std::cout << msg;

    svr.listen_after_bind();
}
} // namespace webserver
