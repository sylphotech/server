#define _WIN32_WINNT 0x600
#define G8_HOST

#include <iostream>
#include <unordered_map>
#include <string>
#include <fstream>
#include <thread>

// includes winsock2 so must go before windows.h
#include <yhirose/httplib.h>

#ifdef _WIN32
#include <windows.h>
#endif

#define CONSOLE_DEFINITION
#define IO_DEFINITION
#include "common/console.h"
#include "common/io.h"

#include "g8/web_server.h"
#include "g8/udp_server.h"
#include "g8/plugin.hpp"

int main (int argc, char* argv[])
{
    plugin::load("plug.dll");

    // working directory
    std::string path = "";

    // map for holding flags
    std::unordered_map<std::string, bool> flags;

    /* parse flags */
    for (int i = 1; i < argc; i++)
    {
        // convert arg to string
        auto arg = std::string(argv[i]);

        // check if path to read from
        std::ifstream ifs(argv[i]);

        if (ifs) path = arg;
        else if (arg == ".")
        {
            #ifdef _WIN32

            char buf[MAX_PATH];
            GetCurrentDirectoryA(256, buf);

            #else

            char buf[PATH_MAX];
            if (getcwd(buf, sizeof(buf)) == NULL)
            {
               std::cout << "Could not get current working directory. Please ";
               std::cout << "provide the path to the project when running G8 ";
               std::cout << "(use a command line).";
               return 0;
            }

            #endif

            path = std::string(buf) + '\\';
        }

        // headless flag
        else if(arg == "--headless")
            flags.insert(std::make_pair<std::string, bool>("headless", true));

        // undefined flag (probably typo)
        else std::cout << "Unknown flag '" << argv[i] << "'" << std::endl;
    }

    if (path == "")
    {
        std::cout << "NO PATH!!" << std::endl;
        // TODO launch project create/select dialog
        return 0;
    }

    std::cout << "Project path [" << path << "]" << std::endl;

    /* launch components */
    terminal::init();

    std::thread websv(webserver::start, path);
    std::thread udpsv(udpserver::start);

    websv.join();
    udpsv.join();

    return 0;
}
