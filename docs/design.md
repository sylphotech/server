# Resource Bundles
G8 games use a single scene, and load/unload resources as needed/unneeded to make
the views required.

Resources are all assigned ID's, and these ID's can be collected together in a
bundle called resource bundles. Resource bundles are a convenient method for loading or unloading multiple resources.

## Scenes
Resource Bundles can be used to implement the traditional "scene" setup. Simply bundle all the resources in a scene together. To transition scenes you'd then unload one bundle, and load the next.

## Views
The Resource Bundle setup also works for the more flexible "view" setup. A view setup is when all visible groups of items, such as menu's and such, are grouped
and presented when needed. For example, in a shooter, the HUD would be one view,
which is layed overtop of the gameplay view. If the developer wanted to hide
the HUD they could disable the HUD view.

This is the setup G8 promotes, but in implementing Resource Bundles, G8 remains
flexible.

# Server operation
During editing, the server's big job is synchronizing the active resources, and
managing update/event code.

# Stream Mode
Event code and all manipulation code is run on the server in the update "loop".
As developers in the editor modify code, it is re-compiled as a dll, and swapped
in. If need be, the entire codebase may be recompiled and swapped.

For the rendering all GL calls are effectively streamed to the client, such that
only textures, shaders, and buffers are sent over. The client simply obeys the
calls made from the server, and acts as the GL canvas for the operations
performed on the server. This system is cross-platform and leaves the client
needing no support for compiling.

# Local Mode
In Local Mode, the server is effectively baked into the client, eliminating the
layer of transport. GL calls are made immediately.
